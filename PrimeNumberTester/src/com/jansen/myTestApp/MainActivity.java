package com.jansen.myTestApp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	private final int DARK_GREEN = 0xFF006600;
	EditText inputNumber;
	TextView result;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
//      Show numeric keyboard.
//        inputNumber = (EditText)findViewById(R.id.editTextInputNumber);
//        inputNumber.setRawInputType(InputType.TYPE_CLASS_NUMBER);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
    	
        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            
            return rootView;
        }
    }
    
    public void calculateButtonClick( View view ) {
    	result = (TextView)findViewById(R.id.textViewResult);
    	inputNumber = (EditText)findViewById(R.id.editTextInputNumber);
    	
    	if( checkPrimeNumber(Integer.parseInt(inputNumber.getText().toString())) ) {
    		result.setText(inputNumber.getText() + " � um n�mero primo!");
    		result.setTextColor(DARK_GREEN);
    	} else {
    		result.setText(inputNumber.getText() + " n�o � primo!");
    		result.setTextColor(Color.RED);
    	}	
    }
    
    public Boolean checkPrimeNumber(int number) {
    	
    	Boolean isPrime = true;
    	
    	for( int i = 2 ; i <= number / 2 ; i++ ) {
    		if (number % i == 0) {
        		isPrime = false;
        	}
    	}
    	
    	return isPrime;
	}
}
